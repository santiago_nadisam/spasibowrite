package com.nadisam.spasibo.write;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

class LetterFragmentAdapter extends FragmentStatePagerAdapter
{
    private Context                   mContext;
    // Number of fragments
    private int                       mCount    = 33;

    public LetterFragmentAdapter(FragmentManager fm, Context context)
    {
        super(fm);

        // Save parent activity context
        mContext = context;
    }

    @Override
    public Fragment getItem(int position)
    {
        // Save current position
        return LetterFragment.newInstance(position, this, mContext);
    }

    // @Override
    public int getItemPosition(Object object)
    {
        return POSITION_NONE;
    }

    @Override
    public int getCount()
    {
        return mCount;
    }
}