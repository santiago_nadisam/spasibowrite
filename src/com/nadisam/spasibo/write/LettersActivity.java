package com.nadisam.spasibo.write;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuInflater;

public class LettersActivity extends FragmentActivity
{

    private LetterFragmentAdapter     mAdapter;
    private ViewPager                 mPager;
    private int                       mCount = 33;
    private int                       currentPage;
    // Sound Pool
    private SoundPool                 soundPool;
    private SparseIntArray            soundsMap;
    private FingerView                mFingerView;
    private Core core;
    private Activity context;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.letters_layout);
        
        this.context = this;
        
        // Set stream music type
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // Get Core
        core = (Core)this.getApplicationContext();
        
        mAdapter = new LetterFragmentAdapter(getSupportFragmentManager(), this);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        PageListener pageListener = new PageListener();
        mPager.setOnPageChangeListener(pageListener);

        // Check if AddContactDialog was started from contact or sos contact activities
        Intent intent = getIntent();
        int position = intent.getIntExtra("abc.position", 0);
        mPager.setCurrentItem(position);

        // Load Pool
        core.playSound(position);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        return true;
    }

    private class PageListener extends SimpleOnPageChangeListener
    {
        public void onPageSelected(int position)
        {
            currentPage = position;
            core.playSound(position);
            Logger.debug("Pagger: " + String.valueOf(currentPage));
            
            // Get finger view
            mFingerView = (FingerView) findViewById(R.id.finger);
            if (mFingerView != null)
            {
                // Get view size
                int width = context.getWindow().getDecorView().getWidth();
                int height = context.getWindow().getDecorView().getHeight();
                
                mFingerView.clear(width, height);
            }
        }
    }
}