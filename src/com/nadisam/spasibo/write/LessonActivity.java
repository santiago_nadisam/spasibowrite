package com.nadisam.spasibo.write;

import java.io.IOException;
import java.util.Locale;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LessonActivity extends Activity
{
    private WebView    webView = null;
    public MediaPlayer mp;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson_webview);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Load web on webview
        webView = (WebView) findViewById(R.id.webview);
        displayWebPage(webView, "es", 0);

        // Override load
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                if (url.endsWith(".mp3"))
                {
                    url = url.replace("file:///android_asset/", "");
                    Log.i("MyWebViewClient", url);
                    try
                    {
                        AssetFileDescriptor afd = LessonActivity.this.getAssets().openFd(url);
                        mp = new MediaPlayer();
                        mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                        afd.close();
                        mp.prepare();
                        mp.start();
                    }
                    catch (IllegalArgumentException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (IllegalStateException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (IOException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    return true;
                }
                else
                {
                    return true;
                }
            }

        });
    }

    private void displayWebPage(WebView webview, String lang, int page)
    {
        String html = "<html><body>Hello, World!</body></html>";
        String mime = "text/html";
        String encoding = "utf-8";

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setSupportZoom(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        String locale = Locale.getDefault().getLanguage();
        AssetManager am = this.getAssets();

        if (0 == locale.compareTo("es"))
        {
            // webview.loadDataWithBaseURL("file:///android_asset/pdf/pdf-es/", "<img src=\"page_es_" + String.valueOf(page + 1) + ".png\" width=\"100%\"/>", "text/html", "utf-8", null);
            // webview.loadDataWithBaseURL(null, html, mime, encoding, null);
        }
        else
        {
            // webview.loadDataWithBaseURL(null, html, mime, encoding, null);
            // webview.loadDataWithBaseURL("file:///android_asset/pdf/pdf-en/", "<img src=\"" + String.valueOf(page) + "-firstaid-pdf.png\" width=\"100%\"/>", "text/html", "utf-8", null);
        }

        webview.loadUrl("file:///android_asset/index.html");
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
    }
}
