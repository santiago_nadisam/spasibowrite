package com.nadisam.spasibo.write;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public final class LetterFragment extends Fragment
{
    private static final String   KEY_CONTENT = "TestFragment:Content";
    private int                   mPosition   = 0;
    private ImageView             imageView;
    private TextView              textView;
    private String[]    mUpLetters  = { "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я" };
    private String[]    mLowLetters = { "а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "", "", "", "э", "ю", "я" };


    public static LetterFragment newInstance(int position, LetterFragmentAdapter context, Context activityContext)
    {
        LetterFragment fragment = new LetterFragment();

        fragment.mPosition = position;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View myView = null;

        // Retrieve saved state
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT))
        {
            mPosition = savedInstanceState.getInt(KEY_CONTENT);
        }
        
        myView = inflater.inflate(R.layout.letter, container, false);
        try
        {
            changeLetter(myView, this.mPosition);
        }
        catch (Exception e)
        {
            Logger.error(this.toString(), e);
        }

        // Set onTouch listener
        return myView;
    }

    private void changeLetter(View myView, int possition)
    {
        imageView = (ImageView) myView.findViewById(R.id.letterImage);
        textView = (TextView) myView.findViewById(R.id.letterText);

        int id = getResources().getIdentifier("com.nadisam.spasibo.write:drawable/a" + String.valueOf(possition + 1), null, null);
        imageView.setImageResource(id);
        textView.setText(mUpLetters[possition] + " " + mLowLetters[possition]);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CONTENT, mPosition);
    }
}
