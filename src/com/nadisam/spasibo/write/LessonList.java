package com.nadisam.spasibo.write;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class LessonList extends SherlockActivity
{
    private String[] lessons;
    private ListView listView;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson_list);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        // Create the resources instance
        Resources res = getResources();

        // To get the string array associated with particular resource ID
        lessons = res.getStringArray(R.array.lessons);

        // Get list view
        listView = (ListView) findViewById(R.id.lessons_list);

        // Creates list view
        listView.setAdapter(new EfficientAdapterContacts(this));
        listView.setCacheColorHint(0x000000); // Evita que se ponga el fondo negro cuando hay una imagen de fondo

        // Add listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                // TODO Auto-generated method stub
                Toast.makeText(LessonList.this, "Position: " + String.valueOf(position), Toast.LENGTH_LONG).show();
                Intent intentLesson = new Intent(LessonList.this, LessonActivity.class);
                startActivity(intentLesson);
            }
        });
    }

    private class EfficientAdapterContacts extends BaseAdapter
    {
        private LayoutInflater mInflater;
        private Bitmap         mIcon;

        public EfficientAdapterContacts(Context context)
        {
            // Cache the LayoutInflate to avoid asking for a new one each time.
            mInflater = LayoutInflater.from(context);

            // Icons bound to the rows.
            mIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
        }

        /**
         * The number of items in the list is determined by the number of speeches in our array.
         * 
         * @see android.widget.ListAdapter#getCount()
         */
        public int getCount()
        {
            if (null != lessons)
            {
                return lessons.length;
            }
            else
            {
                return 0;
            }
        }

        /**
         * Since the data comes from an array, just returning the index is sufficient to get at the data. If we were using a more complex data structure, we would return whatever object represents one row in the list.
         * 
         * @see android.widget.ListAdapter#getItem(int)
         */
        public Object getItem(int position)
        {
            return position;
        }

        /**
         * Use the array index as a unique id.
         * 
         * @see android.widget.ListAdapter#getItemId(int)
         */
        public long getItemId(int position)
        {
            return position;
        }

        /**
         * Make a view to hold each row.
         * 
         * @see android.widget.ListAdapter#getView(int, android.view.View, android.view.ViewGroup)
         */
        public View getView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder;

            if (convertView == null)
            {
                convertView = mInflater.inflate(R.layout.lesson_list_item, null);

                // Creates a ViewHolder and store references to the two children
                // views we want to bind data to.
                holder = new ViewHolder();
                holder.text = (TextView) convertView.findViewById(R.id.text_contact);
                holder.textNumber = (TextView) convertView.findViewById(R.id.text_contact_number);
                holder.textType = (TextView) convertView.findViewById(R.id.text_contact_type);
                holder.icon = (ImageView) convertView.findViewById(R.id.icon_list_contact);

                convertView.setTag(holder);
            }
            else
            {
                // Get the ViewHolder back to get fast access to the TextView
                // and the ImageView.
                holder = (ViewHolder) convertView.getTag();
            }

            // Bind the data efficiently with the holder.
            holder.text.setText("Lesson " + position);
            holder.textNumber.setText(lessons[position]);
            // holder.textType.setText(mContacts.get(position).getType());
            holder.icon.setImageBitmap(mIcon);
            return convertView;
        }

        class ViewHolder
        {
            TextView  text;
            TextView  textNumber;
            TextView  textType;
            ImageView icon;
        }
    }
}
