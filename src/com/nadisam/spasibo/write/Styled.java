package com.nadisam.spasibo.write;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;

public class Styled extends SherlockActivity
{

    // Components dimensions
    private int      mTextSize = 18;
    private GridView mGridview;
    private int      mPosition;
    private int      mNumRows  = 5;
    private int      mBoxSize;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.text);

        // Set grid view
        mGridview = (GridView) findViewById(R.id.gridAbc);
        mGridview.setAdapter(new ButtonAdapter(this));

        // Get screen dimensions
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        mBoxSize = Math.round(width / mNumRows);

        // This is a workaround for http://b.android.com/15340 from
        // http://stackoverflow.com/a/5852198/132047
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            BitmapDrawable bg = (BitmapDrawable) getResources().getDrawable(R.drawable.bg_striped);
            bg.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
            getSupportActionBar().setBackgroundDrawable(bg);

            BitmapDrawable bgSplit = (BitmapDrawable) getResources().getDrawable(R.drawable.bg_striped_split_img);
            bgSplit.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
            getSupportActionBar().setSplitBackgroundDrawable(bgSplit);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //menu.add("Save").setIcon(R.drawable.ic_compose).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        //menu.add("Search").setIcon(R.drawable.ic_search).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        return super.onCreateOptionsMenu(menu);
    }

    public class ButtonAdapter extends BaseAdapter
    {
        private Context mContext;

        public ButtonAdapter(Context c)
        {
            mContext = c;
        }

        public int getCount()
        {
            return 33;
        }

        public Object getItem(int position)
        {
            return null;
        }

        public long getItemId(int position)
        {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        // position [0-MAX]
        public View getView(int position, View convertView, ViewGroup parent)
        {
            mPosition = position;
            Button buttonView;
            if (convertView == null)
            {
                buttonView = new Button(mContext);
                buttonView.setLayoutParams(new GridView.LayoutParams(mBoxSize, mBoxSize));
            }
            else
            {
                buttonView = (Button) convertView;
            }

            // If this day is today
            buttonView.setText(mUpLetters[position] + " " + mLowLetters[position]);
            buttonView.setTextSize(TypedValue.COMPLEX_UNIT_SP, mTextSize); // Size of the button text
            buttonView.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v)
                {
                    Intent intent = new Intent(mContext, LettersActivity.class);
                    intent.putExtra("abc.position", getIndex(((Button) v).getText().toString()));
                    startActivity(intent);
                }
            });

            return buttonView;

        }

        private int getIndex(String value)
        {
            int numLetters = mUpLetters.length;
            for (int i = 0; i < numLetters; i++)
            {
                if (true == value.contains(mUpLetters[i]))
                {
                    return i;
                }
            }
            return 0;
        }

        // references to our images
        private String[]    mUpLetters  = { "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я" };
        private String[]    mLowLetters = { "а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "", "", "", "э", "ю", "я" };

        private Set<String> VALUES      = new HashSet<String>(Arrays.asList(mUpLetters));

    }

    public void onClick(View v)
    {
        // TODO Auto-generated method stub
    }
}
