package com.nadisam.spasibo.write;

import android.util.Log;

public class Logger
{

	private static final String	TAG	= "com.imaxdi.ubeat";

	public static void debug(String msg)
	{
		Log.d(TAG, msg);
	}

	public static void error(String msg, Exception e)
	{
		Log.e(TAG, msg);
		e.printStackTrace();
		// FIXME ErrorReporter.getInstance().handleException(e);
	}

	public static void warning(String msg)
	{
		Log.w(TAG, msg);
	}
}
