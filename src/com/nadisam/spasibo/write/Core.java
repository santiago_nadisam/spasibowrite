package com.nadisam.spasibo.write;

import android.app.Application;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.SparseIntArray;

public class Core extends Application
{
    // Sound Pool
    private SoundPool                 soundPool;
    private SparseIntArray            soundsMap;
    private int                       mCount = 33;
    
    public void startSoundPool()
    {
        soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 100);
        soundsMap = new SparseIntArray();
        for (int i = 0; i < mCount; i++)
        {
            int id = getResources().getIdentifier("letter_" + String.valueOf(i+1), "raw", getPackageName());
            soundsMap.append(i, soundPool.load(this, id, 1));
        }
    }
    
    public void playSound(int letter)
    {
        try
        {
            if (letter >= 0)
            {
                AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
                float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                float volume = streamVolumeCurrent / streamVolumeMax;
                soundPool.play(soundsMap.get(letter), volume, volume, 1, 0, 1);
            }
        }
        catch (Exception e)
        {
            Logger.error(this.toString(), e);
        }
    }
}
