package com.nadisam.spasibo.write;

import android.content.Context;
import android.content.Intent;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class SpasiboActivity extends SherlockActivity implements OnClickListener
{
    private Button btAlphabet;
    private Button btSyllable;
    private Context mContext;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main_menu);
        mContext = this;

        // Set stream music type
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        // Get Core
        Core core = (Core)this.getApplicationContext();
        core.startSoundPool();
        
        // Alphabet button
        btAlphabet = (Button) findViewById(R.id.btAbc);
        btAlphabet.setOnClickListener(this);
        // Syllable button
        btSyllable = (Button) findViewById(R.id.btSyllable);
        btSyllable.setOnClickListener(this);
        
        // This is a workaround for http://b.android.com/15340 from
        // http://stackoverflow.com/a/5852198/132047
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            BitmapDrawable bg = (BitmapDrawable) getResources().getDrawable(R.drawable.bg_striped);
            bg.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
            getSupportActionBar().setBackgroundDrawable(bg);

            BitmapDrawable bgSplit = (BitmapDrawable) getResources().getDrawable(R.drawable.bg_striped_split_img);
            bgSplit.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
            getSupportActionBar().setSplitBackgroundDrawable(bgSplit);
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //menu.add("Save").setIcon(R.drawable.ic_compose).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        //menu.add("Search").setIcon(R.drawable.ic_search).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        //menu.add("Refresh").setIcon(R.drawable.ic_refresh).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Main screen buttons click
     */
    public void onClick(View v)
    {
        // If ABC button is pressed
        if (v == btAlphabet)
        {
            Intent intent = new Intent(mContext, Styled.class); 
            startActivity(intent); 
        }
        // If Syllable button is pressed
        if (v == btSyllable)
        {
            Intent intent = new Intent(mContext, LessonList.class); 
            startActivity(intent); 
        }
    }
}