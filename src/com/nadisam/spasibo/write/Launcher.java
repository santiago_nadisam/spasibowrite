package com.nadisam.spasibo.write;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

public class Launcher extends Activity
{
    private ProgressDialog  pd;
    private Thread          mSplashThread;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        splashScreen();
        
    }
    
    /**
     * Shows mBeat splash screen
     */
    public void splashScreen()
    {
        // Splash screen view
        setContentView(R.layout.splash);

        // The thread to wait for splash screen events
        mSplashThread = new Thread() 
        {
            @Override
            public void run()
            {
                try
                {
                    synchronized (this)
                    {
                        // Wait given period of time or exit on touch
                        wait(2000);
                    }
                }
                catch (InterruptedException ex)
                {
                    ex.printStackTrace();
                }

                // Run next activity
                Intent loginIntent = new Intent();
                loginIntent.setClass(Launcher.this, SpasiboActivity.class);
                startActivity(loginIntent);
                finish();
            }
        };

        mSplashThread.start();
    }
    
    @Override
    protected void onResume() 
    {
        super.onResume();
    }

    @Override
    /**
     * Called when the Data tab is closed
     */
    protected void onStop()
    {
        super.onStop();
    }
}
