package com.nadisam.spasibo.write;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

public class Abc extends SherlockActivity implements OnClickListener
{
    // Components dimensions
    private int      mTextSize = 18;
    private GridView mGridview;
    private int      mPosition;
    private int      mNumRows  = 5;
    private int      mBoxSize;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.abecedary);

        // Set grid view
        mGridview = (GridView) findViewById(R.id.gridAbc);
        mGridview.setAdapter(new ButtonAdapter(this));

        // Get screen dimensions
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        mBoxSize = Math.round(width / mNumRows);
    }

    public class ButtonAdapter extends BaseAdapter
    {
        private Context mContext;

        public ButtonAdapter(Context c)
        {
            mContext = c;
        }

        public int getCount()
        {
            return 33;
        }

        public Object getItem(int position)
        {
            return null;
        }

        public long getItemId(int position)
        {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        // position [0-MAX]
        public View getView(int position, View convertView, ViewGroup parent)
        {
            if (position < 33)
            {
                mPosition = position;
                Button buttonView;
                if (convertView == null)
                {
                    buttonView = new Button(mContext);
                    buttonView.setLayoutParams(new GridView.LayoutParams(mBoxSize, mBoxSize));
                }
                else
                {
                    buttonView = (Button) convertView;
                }

                // If this day is today
                buttonView.setTextSize(mTextSize); // Size of the button text
                buttonView.setText(mUpLetters[position] + " " + mLowLetters[position]);
                buttonView.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v)
                    {
                        Intent intent = new Intent(mContext, LettersActivity.class);
                        intent.putExtra("abc.position", getIndex(((Button) v).getText().toString()));
                        startActivity(intent);
                    }
                });

                return buttonView;
            }
            return null;
        }

        private int getIndex(String value)
        {
            int numLetters = mUpLetters.length;
            for (int i = 0; i < numLetters; i++)
            {
                if (true == value.contains(mUpLetters[i]))
                {
                    return i;
                }
            }
            return 0;
        }

        // references to our images
        private String[]    mUpLetters  = { "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я" };
        private String[]    mLowLetters = { "а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "", "", "", "э", "ю", "я" };

        private Set<String> VALUES      = new HashSet<String>(Arrays.asList(mUpLetters));

    }

    public void onClick(View v)
    {
        // TODO Auto-generated method stub
    }
}
